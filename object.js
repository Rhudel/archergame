var spriteObject=
{
    sourceX: 0,
    sourceY: 0,
    sourceWidth: 90,
    sourceHeight:63,

    x: 0,
    y: 0,
    width: 90,
    height: 63,

    vx: 0,
    vy: 0,

    rotation: 0,
    speed: 2,

    visible: true,
    scrollable: true,

    //physics property
    accelerationX: 0,
    accelerationY: 0,
    speedLimit: 5,
    friction: 0.97,
    bounce: -0.7,
    gravity: 0.3,


    //game property
    inGround: unified,
    jumpForce: -10,

    centerX: function()
    {
        return this.x+this.width/2;
    },
    centerY: function()
    {
        return this.y+this.height/2;
    },
    halfWidth: function()
    {
        return this.width/2;
    },
    halfHeight: function()
    {
        return this.height/2;
    }
};

//message Object
var messageObject=
{
    x: 0,
    y: 0,
    visible: true,
    text: "message",
    font: "normal bold 34px OCR Std",
    fillStyle: "black",
    textBaseline: "top"
};