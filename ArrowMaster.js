(function() {

    //canvas
    var canvas=document.querySelector("canvas");
    var drawingSurface=canvas.getContext("2d");

    canvasResize();
    window.addEventListener("resize",canvasResize,false);
        function canvasResize()
        {
            canvas.width=document.width || document.body.clientWidth;
            canvas.height=document.height || document.body.clientHeight;
        }

        //array
        var sprites = [];
        var assetsToLoad = [];
        var arrows = [];
        var enemies = [];
        var innerMeters = [];
        var outerMeters = [];
        var messages = [];


        //gameVariable
        var assetsHasLoaded = 0;
        var angle = 0;
        var SIZE = 90;

        var hold = false;
        var fire = false;
        var holdTimer = 0;

        var arrowSpeed = 7;
        var enemySpeed = 0.5;

        var score = 0;

        //mouse variable
        var mouseX = 0;
        var mouseY = 0;
        var centerX = 0;
        var centerY = 0;

        //gameState
        var LOADING = 0;
        var PLAYING = 1;
        var OVER = 2;
        var gameState = LOADING;

        //mouseListener
        canvas.addEventListener("mousemove",mousemoveHandler,false);
        canvas.addEventListener("mousedown",mousedownHandler,false);
        canvas.addEventListener("mouseup",mouseupHandler,false);
        canvas.style.cursor = "none";
        function mousemoveHandler(event)
        {
            mouseX = event.pageX-canvas.offsetLeft;
            mouseY = event.pageY-canvas.offsetTop;
        }
        function mousedownHandler(event)
        {
            centerX = event.pageX-canvas.offsetLeft;
            centerY = event.pageY-canvas.offsetTop;
            hold = true;
        }
        function mouseupHandler(event)
        {
            hold = false;
            if(fire)
            {
                fireArrow();
            }
        }

        //keyCode
        var LEFT = 65;
        var UP = 87;
        var RIGHT = 68;
        var DOWN = 83;

        //direction
        var moveLeft = false;
        var moveRight = false;
        var moveUp = false;
        var moveDown = false;

        //keyListener
         window.addEventListener("keydown",function(event)
         {
                switch(event.keyCode)
                {
                    case UP:
                    moveUp = true;
                    break;

                    case DOWN:
                    moveDown = true;
                    break;

                    case LEFT:
                    moveLeft = true;
                    break;

                    case RIGHT:
                    moveRight = true;
                    break;
                }
         },false);
         
         window.addEventListener("keyup",function(event)
         {
                switch(event.keyCode)
                {
                    case UP:
                    moveUp = false;
                    break;

                    case DOWN:
                    moveDown = false;
                    break;

                    case LEFT:
                    moveLeft = false;
                    break;

                    case RIGHT:
                    moveRight = false;
                    break;
                }
         },false);  

         //player
         var player = Object.create(spriteObject);
         player.x = canvas.width/2-player.width/2;
         player.y = canvas.height/2-player.height/2;
         sprites.push(player);

         //player healthMeter
         //healthMeter
         var playerInnerMeter = Object.create(spriteObject);
         playerInnerMeter.sourceX = 136;
         playerInnerMeter.sourceY = 64;
         playerInnerMeter.sourceWidth = 90;
         playerInnerMeter.sourceHeight = 10;
         playerInnerMeter.width = 90;
         playerInnerMeter.height = 10;
         sprites.push(playerInnerMeter);

         var playerOuterMeter = Object.create(spriteObject);
         playerOuterMeter.sourceX = 136;
         playerOuterMeter.sourceY = 74;
         playerOuterMeter.sourceWidth = 90;
         playerOuterMeter.sourceHeight = 10;
         playerOuterMeter.width = 90;
         playerOuterMeter.height = 10;
         sprites.push(playerOuterMeter);

         //enemyObject
         var enemyObject = Object.create(spriteObject);
         enemyObject.angle = 0;
         enemyObject.numberOfFrame = 4;
         enemyObject.currentFrame = 0;
         enemyObject.frameTimer = 0;
         enemyObject.fireTimer = 0;
         enemyObject.fireFrequency = 30;
         enemyObject.arrows = [];
         enemyObject.hit = false;
         enemyObject.animationUpdate = function()
         {
                this.frameTimer++
                if(this.frameTimer%13===0)
                {
                    if(this.currentFrame<this.numberOfFrame)
                    {
                        this.sourceX = this.width*this.currentFrame;
                    }
                    else  
                    {
                        this.sourceX = 0;
                        this.currentFrame = 0;
                        this.frameTimer = 0;
                    }
                    this.currentFrame++;
                }
         };

         //scoreMessage
         var scoreMessage = Object.create(messageOject);
         scoreMessage.x = 20;
         scoreMessage.y = 20;
         scoreMessage.text = "";
         messages.push(scoreMessage);


         //gameOverMessage
         var gameOverMessage = Object.create(messageOject);
         gameOverMessage.x = canvas.width/2-150;
         gameOverMessage.y = canvas.height/2-10;
         gameOverMessage.text = "GAME OVER";
         gameOverMessage.font = "normal bold 50px OCR A Std";
         gameOverMessage.fillStyle = "black";
         gameOverMessage.visible = false;
         messages.push(gameOverMessage);

         //image
         var image = new Image();
         image.addEventListener("load",loadHandler,false);
         image.src = "tileSheet.png";////////////////////////////////////////
         assetsToLoad.push(image);

         function loadHandler()
         {
             assetsHasLoaded++;
             if(assetsHasLoaded===assetsToLoad.length)
             {
                 image.removeEventListener("load",loadHandler,false);

                 //change gameState
                 gameState = PLAYING;
             }
         }

         update();

         function update()
         {
            //requestAnimation
            requestAnimationFrame(update,canvas);

            switch(gameState)
            {
                case LOADING:
                console.log("Loading.....");
                break;

                case PLAYING:
                playGame();
                break;

                case OVER:
                endGame();
                break;
            }
            render();
         }

         function playGame()
         {
             //LEFT
             if(moveLeft && !moveRight)
             {
                 player.vx=-player.speed;
             }

             //RIGHT
             if(moveRight && !moveLeft)
             {
                 player.vx=player.speed;
             }

              //UP
              if(moveUp && !moveDown)
              {
                  player.vy=-player.speed;
              }
 
              //DOWN
              if(moveDown && !moveUp)
              {
                  player.vy=player.speed;
              }

              //if Key is not Pressed
             if(moveLeft && !moveRight)
             {
                 player.vx = 0;
             }
             if(moveUp && !moveDown)
             {
                 player.vy = 0;
             }

             //move
             player.x+=player.vx;
             player.y+=player.vy;

             //rotate
             angle = Math.atan2(mouseY-player.centerY(),mouseX-player.centerX());
             player.rotation = angle*(180/Math.PI)+90;

             //healthMeter
             playerInnerMeter.x = player.x;
             playerInnerMeter.y = player.y-20;

             playerOuterMeter.x = playerInnerMeter.x
             playerOuterMeter.y = playerInnerMeter.y

             //boundary
             if(player.x<0)
             {
                 player.x = 0;
             }
             if(player.y<0)
             {
                 player.y = 0;
             }
             if(player.x+player.width>canvas.width)
             {
                 player.x=canvas.width-player.width;
             }
             if(player.y+player.height>canvas.height)
             {
                 player.y=canvas.height-player.height;
             }


             //fire arrow
             if (hold)
             {
                 holdTimer++
                 if (holdTimer > 7)
                 {
                     player.sourceX = 90;
                     arrowSpeed = 12;
                     fire = true;
                 }
                 if (holdTimer > 15)
                 {
                     player.sourceX = 180;
                     arrowSpeed = 16;
                     fire = true;
                 }
                 else
                 {
                     fire = false;
                 }
             }
             else
             {
                 holdTimer = 0;
                 player.sourceX = 0;
                 arrowSpeed = 7;
             }

             //move arrow 
             for(var i =0; i<arrows.length; i++)
             {
                 var arrow = arrows[i];

                 //apply friction
                 arrow.vx*=arrow.friction;
                 arrow.vy*=arrow.friction;

                 //move 
                 arrow.vx+=arrow.vx;
                 arrow.vy+=arrow.vy;


                 if(arrow.centerX()<0 || arrow.centerX()>canvas.width ||arrow.centerY()<0 || arrow.centerY()>canvas.height)
                 {
                     removeObject (arrow,arrows);
                     removeObject(arrow,sprites);
                     i--;
                 }

                 setTime(remove,1500);
                 function remove ()
                 {
                    removeObject (arrow,arrows);
                    removeObject(arrow,sprites);
                 }
             }

             //create enemy
             if(enemies.length===0)
             {
                 createEnemy();
                 enemySpeed+=0.5;
             }

             //move enemies
             for(var i = 0; i<enemies.length;i++)
             {
                 var enemy = enemies[i];
                 var innerMeter=innerMeters[i];
                 var outerMeter = outerMeters[i];
                 
                 if(!enemy.hit)
                 {
                     //rotate
                     enemy.angle = Math.atan2(player.centerY()-enemy.centerY(),player.centerX()-enemy.centerX());
                     enemy.rotation = enemy.angle*(180/Math.PI)+90;

                     var vx = player.centerX()-enemy.centerX();
                     var vy = player.centerY()-enemy.centerY();

                     var distance = Math.sqrt(vx*vx+vy*vy);

                     //set speed
                     if(distance>200)
                     {
                         enemy.vx = Math.cos(enemy.angle)*enemySpeed;
                         enemy.vy = Math.sin(enemy.angle)*enemySpeed;
                     }
                     else
                     {
                        enemy.vx = 0;
                        enemy.vy = 0;
                     }

                     //move 
                     enemy.x +=enemy.vx;
                     enemy.y +=enemy.vy;

                     //enemy Arrow
                     //animation
                     enemy.fireTimer++;
                     if(enemy.fireTimer>enemy.fireFrequency)
                     {
                         enemy.animationUpdate();

                         if(enemy.currentFrame>3)
                         {
                             fireEnemyArrow(enemy);
                             enemy.fireTimer = 0;
                             enemy.currentFrame = 0;
                         }
                     }
                 }

                 //healthMeter
                 innerMeter.x = enemy.x;
                 innerMeter.y = enemy.y-20;

                 outerMeter.x = innerMeter.x;
                 outerMeter.y = innerMeter.y;

                 //move enemy arrow
                 for(var j =0; i<enemy.arrows.length; j++)
                 {
                     var arrow = enemy.arrows[j];
    
                     //apply friction
                     arrow.vx*=arrow.friction;
                     arrow.vy*=arrow.friction;
    
                     //move 
                     arrow.vx+=arrow.vx;
                     arrow.vy+=arrow.vy;
    
    
                     if(arrow.centerX()<0 || arrow.centerX()>canvas.width ||arrow.centerY()<0 || arrow.centerY()>canvas.height)
                     {
                         removeObject (arrow,enemy.arrows);
                         removeObject(arrow,sprites);
                         j--;
                     }
    
                     setTime(remove,1000);
                     function remove ()
                     {
                        removeObject (arrow,enemy.arrows);
                        removeObject(arrow,sprites);
                        j--;
                     }
                 }
                 
                 //collision between players enemys arrow and player
                 if(hitRectangle(arrow,player))
                 {
                     if(playerInnerMeter.width>0)
                     {
                         playerInnerMeter.width-=10;
                     }
                     if(playerInnerMeter.width<1)
                     {
                         gameState = OVER;
                     }

                     //remove arrow
                     removeObject(arrow,enemy.arrows);
                     removeObject(arrow,sprites);
                     j--;
                 }    
             }

             //collision between player's arrow and enemy
             for (var k=0; k<arrows.length; k++)
             {
                 var arrow = arrow[k];

                 if(hitRectangle(arrow,enemy) && !enemy.hit)
                 {
                        if(innerMeter.width>0)
                        {
                            innnerMeter.width-=10;
                        }
                        if(innerMeter.width<1)
                        {
                            score++;
                            console.log("score"+score);
                            enemy.hit = true;
                            removeEnemy(enemy,innerMeter,outerMeter);
                        }


                        //remove arrow
                        removeObject(arrow,arrows);
                        removeObject(arrow,sprites);
                        k--;
                    }
                }
            }

         //score
         scoreMessage.text = "Kill:" + score;   
        

        function removeEnemy(enemy,innerMeter,outerMeter)
        {
            setTimeout(remove,1000);
            function remove()
            {
                removeObject(enemy,enemies);
                removeObject(enemy,sprites);

                removeObject(innerMeter,innerMeters);
                removeObject(innerMeter,sprites);

                removeObject(outerMeter,outerMeters);
                removeObject(outerMeter,sprites);
            }
        }
        function fireEnemyArrow(enemy)
        {
            //create arrow
            var radius=10;
            var arrow = Object.create(spriteObject);
            arrow.sourceX = 360;
            arrow.sourceWidth = 6;
            arrow.sourceHeight = 40;
            arrow.width = 6;
            arrow.height = 40;

            //
            arrow.x=enemy.centerX()-arrow.halfWidth()+(radius*Math.cos(enemy.angle));
            arrow.y=enemy.centerY()-arrow.halfHeight()+(radius*Math.sin(enemy.angle));
            
            //set speed
            arrow.vx=Math.cos(enemy.angle)*12;
            arrow.vy=Math.sin(enemy.angle)*12;

            //rotate
            arrow.rotation = enemy.angle*(180/Math.PI)+90;

            enemy.arrows.push(arrow);
            sprites.push(arrow);
        }


        function createEnemy()
        {
            var enemy = Object.create(enemyObject);
            var randomDir = Math.floor(Math.random()*4);

            if(randomDir===0)
            {
                //up
                enemy.x =Math.floor(Math.random()*canvas.width);
                enemy.y=-100;
            }
            if(randomDir===1)
            {
                //right
                enemy.x = canvas.width+100;
                enemy.y =Math.floor(Math.random()*canvas.height);
            }
            if(randomDir===2)
            {
                //bottom
                enemy.x =Math.floor(Math.random()*canvas.width);
                enemy.y=canvas.height+100;
            }
            if(randomDir===3)
            {
                //left
                enemy.x=-190;
                enemy.x =Math.floor(Math.random()*canvas.height);
            }

            //healthMeter
            var innerMeter = Object.create(spriteObject);
            innerMeter.sourceX = 136;
            innerMeter.sourceY = 64;
            innerMeter.sourceWidth = 90;
            innerMeter.sourceHeight = 10;
            innerMeter.width = 90;
            innerMeter.height = 10;
            innerMeters.push(innerMeter);
            sprites.push(innerMeter);

            var outerMeter = Object.create(spriteObject);
            outerMeter.sourceX = 136;
            outerMeter.sourceY = 74;
            outerMeter.sourceWidth = 90;
            outerMeter.sourceHeight = 10;
            outerrMeter.width = 90;
            outerMeter.height = 10;
            outerMeters.push(outerMeter);
            sprites.push(outerMeter);

            enemies.push(enemy);
            sprites.push(enemy);
        }

        function fireArrow()
        {
            //create arrow
            var radius = 10;
            var arrow = Object.create(spriteObject);
            arrow.sourceX = 360;
            arrow.sourceWidth = 6;
            arrow.sourceHeight = 40;
            arrow.width = 6;
            arrow.height = 40;


            //
            arrow.x=player.centerX()-arrow.halfWidth()+(radius*Math.cos(angle));
             arrow.y=player.centerY()-arrow.halfHeight()+(radius*Math.sin(angle));
 
            //set speed
            arrow.vx=Math.cos(angle)*arrowSpeed;
            arrow.vy=Math.sin(angle)*arrowSpeed;

            //rotate
            arrow.rotation = angle*(180/Math.PI)+90;

            arrows.push(arrow);
            sprites.push(arrow);
        }

        function removeObject (itemToRemove,array)
        {
            var i = array.indexOf (itemToRemove);
            if(i!==-1)
            {
                array.splice(i,1);
            }
        }

        function endGame()
        {
            gameOverMessage.visible = true;
        }

        function render()
        {
            //clear previous frames
            drawingSurface.clearRect(0,0,canvas.width,canvas.height);

            if(sprites.length!==0)
            {
                for(var i=0; i<sprites.length; i++)
                {
                    var sprite=sprites[i];
                    
                    //save state before rotating the drawingSurface
                    drawingSurface.save();

                    //rotate
                    drawingSurface.translate
                    (
                        Math.floor(sprite.x+sprite.halfWidth()),
                        Math.floor(sprite.y+sprite.halfHeight())
                    );

                    if(sprite.visible)
                    {
                        drawingSurface.drawImage
                        (
                            image,
                            sprite.sourceX,sprite.sourceY,
                            sprite.sourceWidth,sprite.sourceHeight,
                            Math.floor(-sprite.halfWidth()),Math.floor(-sprite.halfHeight()),
                            sprite.width,sprite.height
                        );
                    }

                    drawingSurface.restore();

                }
            }
                    //game Message
                    if(messages.length!==0)
                    {
                        for(var i=0; i<messages.length; i++)
                        {
                            var message = messages[i];
                            if(message.visible)
                            {
                                drawingSurface.font=message.font;
                                drawingSurface.fillStyle=message.fillStyle;
                                drawingSurface.textBaseline=message.textBaseline;
                                drawingSurface.fillText(message.text,message.x,message.y);
                            }
                        }
                    }
                }
            
            }());
            
       
    
        



    

