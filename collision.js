function hitRectangle(r1,r2)
{
    return Math.abs(r1.centerX()-r2.centerX()) < r1.halfWidth()+r2.halfWidth() &&
    Math.abs(r1.centerY()-r2.centerY()) <r1.halfHeight()+r2.halfHeight();
}